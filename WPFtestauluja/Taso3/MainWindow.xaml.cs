﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Taso3
{
    /// <summary>
    /// 18.7.2017 Merja Halonen
    /// WPF erilaisia testailuja
    /// Henkilö-luokka määrittämään tiedot listaboxiin
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public class Henkilo
    {
        private string etunimi;
        private string sukunimi;
        private int ika = 11;
        private string kansallisuus;
        private static Random r = new Random(2017);
        public Henkilo()
        {
            string[] etunimet = { "John", "Jane", "Jack", "Jill", "Jules", "Jake", "Julian", "Jonathan", "Julia", "Judith" };
            string[] sukunimet = { "Doe", "Smith", "Jones", "Taylor", "Brown", "Wilson", "Davis", "Johnson", "Robinson" };
            string[] kansat = { "USA", "Kanada", "Uusi-Seelanti" };
            Ika = r.Next(1, 100);
            Kansallisuus = kansat[r.Next(0, kansat.Length)];
            Etunimi = etunimet[r.Next(0, etunimet.Length)];
            Sukunimi = sukunimet[r.Next(0, sukunimet.Length)];
        }

        public string Kansallisuus
        {
            get { return kansallisuus; }
            set { kansallisuus = value; }
        }

        public string Etunimi
        {
            get { return etunimi; }
            set { etunimi = value; }
        }

        public string Sukunimi
        {
            get { return sukunimi; }
            set { sukunimi = value; }
        }

        public int Ika
        {
            get { return ika; }
            set { if (value > 0 && value <= 100) ika = value; }
        }


    }


    /// <summary>
    /// ValueConverteri listboxin iän tarkistamiseen.
    /// </summary>
    [ValueConversion(typeof(object), typeof(int))]
    public class IkaValueConverter : IValueConverter
    {
        public object Convert(
         object value, Type targetType,
         object parameter, CultureInfo culture)
        {
            int numero = (int)value;

            if (numero < 20)
                return -1;

            if (numero >= 20 && numero <= 60)
                return 0;

             else return 1;
        }

        public object ConvertBack(
         object value, Type targetType,
         object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("Ei toimi näin päin");
        }
    }


    /// <summary>
    /// MultiValueConverter Checkboxin reunuksen värin määrittämiseen sliderien avulla.
    /// </summary>
    public class ColorConverter : IMultiValueConverter
    {
        object IMultiValueConverter.Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return new SolidColorBrush(
                Color.FromArgb(255, Convert.ToByte(values[0]),
                    Convert.ToByte(values[1]), Convert.ToByte(values[2])));
        }

        object[] IMultiValueConverter.ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    /// <summary>
    /// MultiValueConverter tarkistamaan onko kolme checboxia valittu pysty-, vaaka- tai vinosuunnassa.
    /// </summary>
    [ValueConversion(typeof(bool), typeof(String))]
    public class KlikattuConverter : IMultiValueConverter
    {
        // muunnos bool-tyypistä visibility-tyyppiin
        object IMultiValueConverter.Convert(object[] values, Type targetType, object parameter, CultureInfo culture)

        {
            for (int i = 0; i < 28; i++)
            {
                //vaaka
                if (i <= 2 || i >= 5 && i <= 7 || i <= 12 && i >= 10 || i >= 15 && i <= 17 || i >= 20 && i <= 22 || i >= 25 && i <= 27)
                {
                    if (Convert.ToBoolean(values[i]) && Convert.ToBoolean(values[i + 1]) && Convert.ToBoolean(values[i + 2])) return "Valmis rivi";
                }

                //pysty
                if (i == 0 || i <= 4 && i == 15 || i <= 19)
                {
                    if (Convert.ToBoolean(values[i]) && Convert.ToBoolean(values[i + 5]) && Convert.ToBoolean(values[i + 10])) return "Valmis rivi";
                }

                //vino
                if (i == 0 || i <= 18)
                {
                    if (Convert.ToBoolean(values[i]) && Convert.ToBoolean(values[i + 6]) && Convert.ToBoolean(values[i + 12])) return "Valmis rivi";
                }

                //vino
                if (i == 0 || i <= 19)
                {
                    if (Convert.ToBoolean(values[i]) && Convert.ToBoolean(values[i + 4]) && Convert.ToBoolean(values[i + 8])) return "Valmis rivi";

                }

                //viimeiset, mistä muuten menisi taulukon indeksin yli
                if (i >= 18 && i == 19)
                {
                    if (Convert.ToBoolean(values[i]) && Convert.ToBoolean(values[i + 5]) && Convert.ToBoolean(values[i - 5])) return "Valmis rivi";
                }

            }
            return "Peli on kesken";
        }

        object[] IMultiValueConverter.ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    /// <summary>
    /// Pääluokka, joka muodosta listan Henkilöluokan henkilöistä.
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Henkilo> henkilot;

        public List<Henkilo> Henkilot
        {
            get { return henkilot; }
            set { henkilot = value; }
        }

        public MainWindow()
        {
            henkilot = new List<Henkilo>();
            for (int i = 0; i < 16; i++)
            {
                henkilot.Add(new Henkilo());
            }

            InitializeComponent();
        }
    }


}
