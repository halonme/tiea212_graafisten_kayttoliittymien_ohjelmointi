﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PelinappulaControl;
using System.Windows.Media.Animation;

namespace PelialueControl
{
    /// <summary>
    /// Mylly -pelin pelilauta, joka hoitaa pelilogiikan, nappuloiden lisäämisen, siirtelyn, voiton tarkistuksen.
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class Pelialue : UserControl
    {
        //Pelaajat
        Pelaaja player1;
        Pelaaja player2;

        //Pelinappulat
        Pelinappula nappula1;
        Pelinappula nappula2;

        //Pelilaudan pallura
        Ellipse pallura;

        //Pelilaudan paikkojen lista (ympyrät)
        List<Ellipse> palluratLista = new List<Ellipse>();

        //Pelinappuloiden listat
        List<Pelinappula> nappulatLista1 = new List<Pelinappula>();
        List<Pelinappula> nappulatLista2 = new List<Pelinappula>();

        //Tallennettu kaikki mahdolliset siirrot
        private int[,] siirtoTaulu;

        //Tallennettu kaikki mahdolliset mylly vaihtoehdot
        private int[,] myllyTaulu;

        //Tallennettu pelialueen palluroiden grid-paikat
        private int[,] palluratTaulu;

        private MediaPlayer mediaPlayer = new MediaPlayer();

        //Ilmoittaa montako nappulaa on valittu (klikattu)
        private int nappuloitaValittu = 0;

        //Jos nappula valittu, merkitsee, että voi siirtää valitun
        private bool nappulaKlikattu = false;

        //Talteen valittu(klikattu) nappulan
        private Pelinappula valittuNappula;

        //Talteen valitun nappulan vanhempi
        private OwnCanvas valittuParentti;

        //Pelilaudan viimeksi klikatun palluran id
        private int viimeisinUid = 0;

        //True, jos nappula valittu ja sitten peruuttu
        private bool siirtoPeruuttu = false;

        //True, jos kaikki nappulat on laitettu pelilaudalle
        private bool kaikkiNappulat = false;

        private Storyboard myStoryboard;
        TextBlock myTextblock;
        Canvas myCanvas;

        //True, jos jommalla kummalla pelaajalla mylly
        private bool mylly = false;



        /// <summary>
        /// Konstruktori, jossa alustetaan mm. siirtoTaulu ja Myllytaulu sekä animoitu voittajateksti
        /// </summary>
        public Pelialue()
        {
            InitializeComponent();
            SizeChanged += Pelialue_SizeChanged;
            siirtoTaulu = new int[24, 4] { { 1, 7, -1, -1}, { 0, 2, 9, -1 }, { 1, 3, -1, -1 }, { 2, 4, 11, -1 }, { 3, 5, -1, -1 }, { 4, 6, 13, -1 }, { 5, 7, -1, -1 }, { 0, 6, 15, -1 },
            { 9, 15, -1, -1 }, { 1, 8, 10, 17 }, { 9, 11, -1, -1 }, { 3, 10, 12, 19 }, { 11, 13, -1, -1 }, { 5, 21, 12, 14 }, { 13, 15, -1, -1 }, { 7, 8, 23, 14 },
            { 17, 23, -1, -1 }, { 9, 16, 18, -1 }, { 17, 19, -1, -1 }, { 18, 11, 20, -1 }, { 19, 21, -1, -1 }, { 20, 22, 13, -1 }, { 21, 23, -1, -1 }, { 16, 15, 22, -1 }};

            myllyTaulu = new int[16, 3] { {0, 1, 2 }, { 2, 3, 4}, { 4, 5, 6}, { 6, 7, 0},
                                         { 8, 9, 10}, { 10, 11, 12}, { 12, 13, 14}, { 14, 15, 8},
                                         { 16, 17, 18 }, {18, 19, 20}, {20, 21, 22 }, { 22, 23, 16 },
                                         { 1, 9, 17}, { 3, 11, 19}, { 21, 13, 5 }, { 7, 15, 23 }};

            //0 sarake on grid row ja 1 sarake on grid column
            palluratTaulu = new int[24, 2] { { 0, 0}, { 0, 3}, { 0, 6}, { 3, 6}, { 6, 6}, { 6, 3}, { 6, 0}, { 3, 0},
                                          { 1, 1 }, {1, 3 }, {1, 5 }, {3, 5 }, {5, 5 }, {5, 3 }, {5, 1 }, {3, 1 }, {2, 2 },
                                           { 2, 3}, {2, 4 }, {3, 4 }, {4, 4 }, {4, 3 }, {4, 2 }, {3,2 } };

            myTextblock = new TextBlock();
            myTextblock.Name = "myTextblock";
            this.RegisterName(myTextblock.Name, myTextblock);
            myTextblock.Width = 120;
            myTextblock.TextWrapping = TextWrapping.Wrap;
            myTextblock.FontSize = 40;
            myTextblock.FontWeight = FontWeights.Bold;
            myTextblock.Foreground = new SolidColorBrush(Colors.DarkOrange);

            player1 = new Pelaaja();
            player2 = new Pelaaja();
        }


        /// <summary>
        /// Property on true, jos pelaaja 1 on valittu tietokone
        /// </summary>
        public bool Tietokone1
        {
            get { return tietokone1; }
            set { tietokone1 = value; }
        }

        private bool tietokone1 = false;


        /// <summary>
        /// Property on true, jos pelaaja 2 on valittu tietokone
        /// </summary>
        public bool Tietokone2
        {
            get { return tietokone2; }
            set { tietokone2 = value; }
        }

        private bool tietokone2 = false;


        /// <summary>
        /// Property on true, jos kieli on englanti
        /// </summary>
        public bool Enkku
        {
            get { return enkku; }
            set { enkku = value; }
        }

        private bool enkku = false;


        /// <summary>
        /// Property on true, jos kieli on suomi
        /// </summary>
        public bool Suomi
        {
            get { return suomi; }
            set { suomi = value; }
        }

        private bool suomi = false;


        /// <summary>
        /// Property kaikkien nappuloiden määrälle
        /// </summary>
        public int Nappuloita
        {
            get { return nappuloita; }
            set { nappuloita = value; }
        }
        private int nappuloita = 0;


        /// <summary>
        /// Property kumpi pelaaja vuorossa
        /// </summary>
        public int PelaajanVuoro
        {
            get { return pelaajanVuoro; }
            set { pelaajanVuoro = value; }
        }

        private int pelaajanVuoro = 0;


        /// <summary>
        /// Property pelaajien ohjeistamiseen 
        /// </summary>
        public string OhjePelaajalle
        {
            get { return (string)GetValue(OhjePelaajalleProperty); }
            set { SetValue(OhjePelaajalleProperty, value); }
        }

        public static readonly DependencyProperty OhjePelaajalleProperty =
            DependencyProperty.Register("OhjePelaajalle", typeof(string), typeof(Pelialue), new PropertyMetadata(""));


        /// <summary>
        /// Property pelaaja 1 nappulan värille
        /// </summary>
        public SolidColorBrush VariNappulat1
        {
            get { return (SolidColorBrush)GetValue(VariNappulat1Property); }
            set { SetValue(VariNappulat1Property, value); }
        }

        public static readonly DependencyProperty VariNappulat1Property =
            DependencyProperty.Register("VariNappulat1", typeof(SolidColorBrush), typeof(Pelialue), new PropertyMetadata(new SolidColorBrush(Colors.Gray)));


        /// <summary>
        /// Property pelaaja 2 nappulan värille
        /// </summary>
        public SolidColorBrush VariNappulat2
        {
            get { return (SolidColorBrush)GetValue(VariNappulat2Property); }
            set { SetValue(VariNappulat2Property, value); }
        }

        public static readonly DependencyProperty VariNappulat2Property =
            DependencyProperty.Register("VariNappulat2", typeof(SolidColorBrush), typeof(Pelialue), new PropertyMetadata(new SolidColorBrush(Colors.Blue)));


        /// <summary>
        /// Property pelaaja 1 nimelle
        /// </summary>
        public static readonly DependencyProperty Player1NameProperty =
        DependencyProperty.Register(
        "Player1Name",
        typeof(string), // propertyn tietotyyppi
        typeof(Pelialue), // luokka jossa property sijaitsee
        new FrameworkPropertyMetadata("Maija",  // propertyn oletusarvo
        FrameworkPropertyMetadataOptions.AffectsRender, // vaikuttaa luokan ulkoasuun 
        new PropertyChangedCallback(OnValueChanged),  // kutsutaan propertyn arvon muuttumisen jälkeen
        new CoerceValueCallback(MuutaNimi))); // kutsutaan ennen propertyn arvon muutosta

        public string Player1Name
        {
            get { return (string)GetValue(Player1NameProperty); }
            set { SetValue(Player1NameProperty, value); }
        }

        /// <summary>
        /// Kutsutaan ennen property arvon muuttamista. Tarkistaa, jos nimeä ei ole, niin laittaa oletusnimen.
        /// </summary>
        /// <param name="element">WindowSettings</param>
        /// <param name="value">propertyn arvo</param>
        /// <returns></returns>
        private static object MuutaNimi(DependencyObject element, object value)
        {
            string nimi = (string)value;

            return nimi;
        }

        /// <summary>
        /// Propertyn arvon muuttamisen jälkeen. Jos nimeä ei annettu laittaa oletusnimen. 
        /// </summary>
        /// <param name="obj">WindowSettings</param>
        /// <param name="args">tapahtuma</param>
        private static void OnValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            Pelialue o = (Pelialue)obj;
            o.player1.Nimi = args.NewValue.ToString();
        }


        /// <summary>
        /// Property pelaaja 2 nimelle
        /// </summary>
        public static readonly DependencyProperty Player2NameProperty =
        DependencyProperty.Register(
        "Player2Name",
        typeof(string), // propertyn tietotyyppi
        typeof(Pelialue), // luokka jossa property sijaitsee
        new FrameworkPropertyMetadata("Matti",  // propertyn oletusarvo
        FrameworkPropertyMetadataOptions.AffectsRender, // vaikuttaa luokan ulkoasuun 
        new PropertyChangedCallback(OnValueChanged2),  // kutsutaan propertyn arvon muuttumisen jälkeen
        new CoerceValueCallback(MuutaNimi2))); // kutsutaan ennen propertyn arvon muutosta

        public string Player2Name
        {
            get { return (string)GetValue(Player2NameProperty); }
            set { SetValue(Player2NameProperty, value); }
        }


        /// <summary>
        /// Kutsutaan ennen property arvon muuttamista. Tarkistaa, jos nimeä ei ole, niin laittaa oletusnimen.
        /// </summary>
        /// <param name="element">WindowSettings</param>
        /// <param name="value">propertyn arvo</param>
        /// <returns></returns>
        private static object MuutaNimi2(DependencyObject element, object value)
        {
            string nimi = (string)value;

            return nimi;
        }


        /// <summary>
        /// Propertyn arvon muuttamisen jälkeen. Jos nimeä ei annettu laittaa oletusnimen. 
        /// </summary>
        /// <param name="obj">WindowSettings</param>
        /// <param name="args">tapahtuma</param>
        private static void OnValueChanged2(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            Pelialue o = (Pelialue)obj;
            o.player2.Nimi = args.NewValue.ToString();
        }


        /// <summary>
        /// Property, ilmaisee voittajan
        /// </summary>
        public string Voittaja
        {
            get { return (string)GetValue(VoittajaProperty); }
            set { SetValue(VoittajaProperty, value); }
        }

        public static readonly DependencyProperty VoittajaProperty =
            DependencyProperty.Register("Voittaja", typeof(string), typeof(Pelialue), new PropertyMetadata(""));


        /// <summary>
        /// Property, ilmaisee vuorossa olevan pelaajan
        /// </summary>
        public string VuorossaPelaaja
        {
            get { return (string)GetValue(VuorossaPelaajaProperty); }
            set { SetValue(VuorossaPelaajaProperty, value); }
        }

        public static readonly DependencyProperty VuorossaPelaajaProperty =
            DependencyProperty.Register("VuorossaPelaaja", typeof(string), typeof(Pelialue), new PropertyMetadata(""));


        /// <summary>
        /// Property pelaaja 1 nappulan muodolle
        /// </summary>
        public string MuotoNappula1
        {
            get { return (string)GetValue(MuotoNappula1Property); }
            set { SetValue(MuotoNappula1Property, value); }
        }

        public static readonly DependencyProperty MuotoNappula1Property =
            DependencyProperty.Register("MuotoNappula1", typeof(string), typeof(Pelialue), new PropertyMetadata("ellipse"));


        /// <summary>
        /// Propeprty pelaaja 2 nappulan muodolle
        /// </summary>
        public string MuotoNappula2
        {
            get { return (string)GetValue(MuotoNappula2Property); }
            set { SetValue(MuotoNappula2Property, value); }
        }

        public static readonly DependencyProperty MuotoNappula2Property =
            DependencyProperty.Register("MuotoNappula2", typeof(string), typeof(Pelialue), new PropertyMetadata("ellipse"));


        /// <summary>
        /// Arvotaan aloittava pelaaja
        /// </summary>
        public void arvoVuoro()
        {
            player1.Nimi = Player1Name;
            if (Tietokone1 == true) player1.Tyyppi = 1;
            else player1.Tyyppi = 0;
            player1.Nappulat = 0;

            player2.Nimi = Player2Name;
            player2.Tyyppi = 0;
            player2.Nappulat = 0;

            Random rand = new Random();
            PelaajanVuoro = rand.Next(1, 3);
            if (PelaajanVuoro == 1) VuorossaPelaaja = player1.Nimi;
            else VuorossaPelaaja = player2.Nimi;
        }


        /// <summary>
        /// Tyhjentää pelilaudan
        /// </summary>
        public void TyhjennaPelilauta()
        {
            if (Voittaja != "")
            {
                myCanvas.Children.Remove(myTextblock);
                pelilauta.Children.Remove(myCanvas);
                myStoryboard.Remove(this);
            }
            for (int i = 0; i < nappulatLista1.Count; i++)
            {
                nappula1 = nappulatLista1[i];

                OwnCanvas parent = ((Pelinappula)nappula1).Parent as OwnCanvas;
                parent.Children.Remove(nappula1);
            }

            for (int i = 0; i < nappulatLista2.Count; i++)
            {
                nappula2 = nappulatLista2[i];
                OwnCanvas parent = ((Pelinappula)nappula2).Parent as OwnCanvas;
                parent.Children.Remove(nappula2);
            }

            nappulatLista1.Clear();
            nappulatLista2.Clear();
            Nappuloita = 0;
            player1.Nappulat = 0;
            player2.Nappulat = 0;
            nappuloitaValittu = 0;
            nappulaKlikattu = false;
            kaikkiNappulat = false;
            OhjePelaajalle = "";

            arvoVuoro();
        }


        /// <summary>
        /// Tapahtuu, kun pelilaudan ruutua (ympyrää) on klikattu.
        /// Lisää pelinappuloita pelilaudalle ja tarkistaa, onko tehty siirto laillinen, jos on, niin siirtää nappulan,
        /// muuten värjää paikan punaiseksi.
        /// </summary>
        /// <param name="sender">Pelilaudan paikka (ympyrä)</param>
        /// <param name="e">Hiiren klikkaustapahtuma</param>
        private void PalluraKlikattu(object sender, MouseButtonEventArgs e)
        {
            pallura = (Ellipse)sender;
            palluratLista.Add(pallura);
            OwnCanvas parentti = ((Ellipse)sender).Parent as OwnCanvas;
            siirtoPeruuttu = false;

            if (Nappuloita == 18) kaikkiNappulat = true;


            if (nappulaKlikattu == true)
            {
                if (player1.Nappulat <= 3 && valittuNappula.Name == player1.Nimi) valittuPoista();
                else if (player2.Nappulat <= 3 && valittuNappula.Name == player2.Nimi) valittuPoista();

                else if (int.Parse(pallura.Uid) == siirtoTaulu[int.Parse(valittuNappula.Uid), 0] ||
                    int.Parse(pallura.Uid) == siirtoTaulu[int.Parse(valittuNappula.Uid), 1] ||
                    int.Parse(pallura.Uid) == siirtoTaulu[int.Parse(valittuNappula.Uid), 2] ||
                    int.Parse(pallura.Uid) == siirtoTaulu[int.Parse(valittuNappula.Uid), 3]) valittuPoista();

                else
                {
                    pallura.Fill = new SolidColorBrush(Colors.Red);
                    if (Enkku == true) OhjePelaajalle = "Transfer is not allowed, select the allowed item";
                    else OhjePelaajalle = "Siirto ei ole sallittu, valitse sallittu kohta";
                }

            }

            if (Nappuloita < 18)
            {

                if (VuorossaPelaaja.Equals(player1.Nimi))
                {
                    if (player1.Tyyppi == 1) return;
                    nappula1 = new PelinappulaControl.Pelinappula();
                    nappula1.NappulanVari = VariNappulat1;
                    nappula1.NappulanKoko = ((parentti.ActualHeight / 2) * (parentti.ActualWidth / 2)) * 0.025;
                    nappula1.NappulanMuoto = MuotoNappula1;
                    if (nappula1.NappulanKoko > 80) nappula1.NappulanKoko = 80;
                    Canvas.SetTop(nappula1, parentti.ActualHeight / 2 - (nappula1.NappulanKoko / 2));
                    Canvas.SetLeft(nappula1, parentti.ActualWidth / 2 - (nappula1.NappulanKoko / 2));
                    nappula1.Name = player1.Nimi;
                    parentti.Children.Add(nappula1);
                    nappulatLista1.Add(nappula1);
                    PelaajanVuoro = 2;
                    Nappuloita++;
                    player1.Nappulat++;
                    nappula1.PreviewMouseLeftButtonDown += Nappula_MouseDown;
                    nappula1.Uid = pallura.Uid;
                    viimeisinUid = int.Parse(nappula1.Uid);
                    palluratMustiksi();
                    tarkistaMylly();
                    tarkistaVoitto();
                    if (mylly == false) VuorossaPelaaja = player2.Nimi; //jos ei myllyä niin vaihtaa vuoron, muuten sama pelaaja

                    return;
                }
                if (VuorossaPelaaja.Equals(player2.Nimi))
                {
                    nappula2 = new PelinappulaControl.Pelinappula();
                    nappula2.NappulanVari = VariNappulat2;
                    nappula2.NappulanKoko = ((parentti.ActualHeight / 2) * (parentti.ActualWidth / 2)) * 0.025;
                    nappula2.NappulanMuoto = MuotoNappula2;
                    if (nappula2.NappulanKoko > 80) nappula2.NappulanKoko = 80;
                    Canvas.SetTop(nappula2, parentti.ActualHeight / 2 - (nappula2.NappulanKoko / 2));
                    Canvas.SetLeft(nappula2, parentti.ActualWidth / 2 - (nappula2.NappulanKoko / 2));
                    nappula2.Name = player2.Nimi;
                    parentti.Children.Add(nappula2);
                    nappulatLista2.Add(nappula2);
                    PelaajanVuoro = 1;
                    Nappuloita++;
                    player2.Nappulat++;
                    nappula2.PreviewMouseLeftButtonDown += Nappula_MouseDown;
                    nappula2.Uid = pallura.Uid;
                    viimeisinUid = int.Parse(nappula2.Uid);
                    palluratMustiksi();
                    tarkistaMylly();
                    tarkistaVoitto();
                    if (mylly == false) VuorossaPelaaja = player1.Nimi;

                    return;
                }
            }
        }


        /// <summary>
        /// Käy kaikki pelilaudan paikat läpi ja värjää ne takaisin mustaksi, kun nappula siirretty lailliseen paikkaan.
        /// </summary>
        public void palluratMustiksi()
        {
            for (int i = 0; i < palluratLista.Count; i++)
            {
                palluratLista[i].Fill = new SolidColorBrush(Colors.Black);
            }
        }


        /// <summary>
        /// Poistaa valitun pelinappulan pelilaudalta ja pelaajan nappulalistasta.
        /// </summary>
        public void valittuPoista()
        {
            valittuParentti.Children.Remove(valittuNappula);

            if (valittuNappula.Name == player1.Nimi)
            {
                nappulatLista1.Remove(valittuNappula);
                player1.Nappulat--;
            }
            else
            {
                nappulatLista2.Remove(valittuNappula);
                player2.Nappulat--;
            }
            nappuloitaValittu--;
            Nappuloita--;
            nappulaKlikattu = false;
            if (Enkku == true) OhjePelaajalle = "The transfer was successful!";
            else OhjePelaajalle = "Siirto onnistui!";
        }



        /// <summary>
        /// Tarkistaa, onko mylly eli kolme nappulaa samalla rivillä.
        /// </summary>
        public void tarkistaMylly()
        {
            int sisaltaa = 0;
            bool siirettyMukana = false;
            if (VuorossaPelaaja.Equals(player1.Nimi))
            {
                for (int k = 0; k < myllyTaulu.GetLength(0); k++)
                {
                    sisaltaa = 0;
                    for (int j = 0; j < myllyTaulu.GetLength(1); j++)
                    {
                        int etsittava = myllyTaulu[k, j];

                        for (int i = 0; i < nappulatLista1.Count; i++)
                        {
                            if (int.Parse(nappulatLista1[i].Uid) == etsittava)
                            {
                                sisaltaa++;
                                if (etsittava == viimeisinUid) siirettyMukana = true;
                            }
                        }
                    }
                    if (sisaltaa == 3 && siirettyMukana == true)
                    {
                        if (Enkku == true) OhjePelaajalle = "Mill, remove the opponent's piece";
                        else OhjePelaajalle = "Mylly, poista vastustajan nappula";
                        mylly = true;
                        break;
                    }
                    else siirettyMukana = false;

                }
            }
            else if (VuorossaPelaaja.Equals(player2.Nimi))
            {
                for (int k = 0; k < myllyTaulu.GetLength(0); k++)
                {
                    sisaltaa = 0;
                    for (int j = 0; j < myllyTaulu.GetLength(1); j++)
                    {
                        int etsittava = myllyTaulu[k, j];

                        for (int i = 0; i < nappulatLista2.Count; i++)
                        {
                            if (int.Parse(nappulatLista2[i].Uid) == etsittava)
                            {
                                sisaltaa++;
                                if (etsittava == viimeisinUid) siirettyMukana = true;
                            }
                        }
                    }
                    if (sisaltaa == 3 && siirettyMukana == true)
                    {
                        if (Enkku == true) OhjePelaajalle = "Mill, remove the opponent's piece";
                        else OhjePelaajalle = "Mylly, poista vastustajan nappula";
                        mylly = true;
                        break;
                    }
                    else siirettyMukana = false;

                }
            }

        }


        /// <summary>
        /// Tapahtuu, kun pelinappulaa on klikattu.
        /// Tarkistaa, onko pelaajan vuoro, jos on, niin kutsuu pelinappulan luokkaa, muuttaakseen valitun nappulan punaiseksi
        /// ja suuremmaksi.
        /// Muuttaa myös valitun nappulan takaisin normaaliksi pelinappula luokan avulla.
        /// Jos mylly, niin poistaa valitun vastustajan pelinappulan.
        /// </summary>
        /// <param name="sender">Valittu pelinappula</param>
        /// <param name="e">Hiiren klikkaustapahtuma</param>
        private void Nappula_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Pelinappula nappula = (Pelinappula)sender;

            OwnCanvas parent = ((Pelinappula)nappula).Parent as OwnCanvas;

            if (nappula == valittuNappula && siirtoPeruuttu == false)
            {
                if (nappulaKlikattu == true && VuorossaPelaaja == player1.Nimi)
                {
                    nappula.NappulanVari = VariNappulat1;

                }
                else
                {
                    nappula.NappulanVari = VariNappulat2;
                }
                nappula.NappulanKoko = nappula.NappulanKoko * 0.90;
                Canvas.SetTop(nappula, parent.ActualHeight / 2 - (nappula.NappulanKoko / 2));
                Canvas.SetLeft(nappula, parent.ActualWidth / 2 - (nappula.NappulanKoko / 2));
                if (Enkku == true) OhjePelaajalle = "The transfer was canceled!";
                else OhjePelaajalle = "Siirto peruttu";
                nappula.Valittu = false;
                nappulaKlikattu = false;
                siirtoPeruuttu = true;
                nappuloitaValittu--;
                palluratMustiksi();

            }
            else if (nappuloitaValittu >= 1)
            {
                nappula.Valittu = false; //Pelinappula luokan property
                if (Enkku == true) OhjePelaajalle = "Moving only to the free space";
                else OhjePelaajalle = "Siirto vain vapaana olevaan kohtaan";
                return;
            }
            else if (VuorossaPelaaja.Equals(nappula.Name) && mylly == false)
            {
                valittuParentti = ((Pelinappula)sender).Parent as OwnCanvas;
                nappulaKlikattu = true;
                valittuNappula = (Pelinappula)sender;
                nappuloitaValittu++;
                valittuNappula.Valittu = true; //Pelinappula luokan property
                nappula.valittuNappula(); //muutos punaiseksi Pelinappula luokassa
                Canvas.SetTop(nappula, parent.ActualHeight / 2 - (nappula.NappulanKoko / 2));
                Canvas.SetLeft(nappula, parent.ActualWidth / 2 - (nappula.NappulanKoko / 2));
                siirtoPeruuttu = false;
                if (Enkku == true) OhjePelaajalle = "Select where to move";
                else OhjePelaajalle = "Valitse kohta, mihin siirretään";
            }

            else if (!VuorossaPelaaja.Equals(nappula.Name) && mylly == true)
            {
                if (nappula.Name == player1.Nimi)
                {
                    parent.Children.Remove(nappula);
                    nappulatLista1.Remove(nappula);
                    player1.Nappulat--;
                    tarkistaVoitto();
                    VuorossaPelaaja = player1.Nimi;

                    OhjePelaajalle = "";
                }
                else
                {
                    parent.Children.Remove(nappula);
                    nappulatLista2.Remove(nappula);
                    player2.Nappulat--;
                    tarkistaVoitto();
                    VuorossaPelaaja = player2.Nimi;

                    OhjePelaajalle = "";
                }
                mylly = false;
            }
            else
            {
                if (mylly == false)
                {
                    if (Enkku == true) OhjePelaajalle = "It's not your turn";
                    else OhjePelaajalle = "Ei ole sinun vuorosi";
                }
                else
                {
                    if (Enkku == true) OhjePelaajalle = "Select the opponent's piece";
                    else OhjePelaajalle = "Valitse vastustajan nappula";
                }
            }
        }


        /// <summary>
        /// Tarkistaa, onko peli päättynyt eli tarkistaa, onko jommalla kummalla pelaajalla 2 nappulaa jäljellä.
        /// Soittaa voittaja -äänen.
        /// </summary>
        public void tarkistaVoitto()
        {
            if (kaikkiNappulat == true && (player1.Nappulat <= 2 || player2.Nappulat <= 2))
            {
                Voittaja = VuorossaPelaaja;
                mediaPlayer.Open(new Uri("Sounds/heres-your-winner.wav", UriKind.Relative));
                mediaPlayer.Play();
                voitti();
            }
        }


        /// <summary>
        /// Jos voitto mahdollinen, niin näyttää voittajan nimen liikkuvassa tekstissä.
        /// </summary>
        public void voitti()
        {
            myCanvas = new Canvas();

            myCanvas.Width = (pelilauta.ActualWidth - myTextblock.Width);
            double leveys = Convert.ToDouble(myCanvas.Width);
            Grid.SetRow(myCanvas, 3);
            pelilauta.Children.Add(myCanvas);

            myTextblock.Text = Voittaja;

            DoubleAnimation myDoubleAnimation = new DoubleAnimation();
            myDoubleAnimation.From = 0.0;
            myDoubleAnimation.To = leveys;
            myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(5));
            myDoubleAnimation.AutoReverse = true;
            myDoubleAnimation.RepeatBehavior = RepeatBehavior.Forever;

            myStoryboard = new Storyboard();
            myStoryboard.Children.Add(myDoubleAnimation);
            Storyboard.SetTargetName(myDoubleAnimation, myTextblock.Name);
            Storyboard.SetTargetProperty(myDoubleAnimation, new PropertyPath(Canvas.LeftProperty));

            myCanvas.Children.Add(myTextblock);
            myStoryboard.Begin(this);

        }


        /// <summary>
        /// Tapahtuu, kun ikkunan kokoa eli myös pelilaudan kokoa muutetaan. 
        /// Suurentaa pelialuetta ja nappuloita ja kohdistaa ne oikealle kohdalle.
        /// </summary>
        /// <param name="sender">Pelialue</param>
        /// <param name="e">Ikkunan koon muutos -tapahtuma</param>
        private void Pelialue_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            for (int i = 0; i < nappulatLista1.Count; i++)
            {
                nappula1 = nappulatLista1[i];
                OwnCanvas parent = ((Pelinappula)nappula1).Parent as OwnCanvas;
                nappula1.NappulanKoko = ((parent.ActualHeight / 2) * (parent.ActualWidth / 2)) * 0.025;
                if (nappula1.NappulanKoko > 80) nappula1.NappulanKoko = 80;
                Canvas.SetTop(nappula1, parent.ActualHeight / 2 - (nappula1.NappulanKoko / 2));
                Canvas.SetLeft(nappula1, parent.ActualWidth / 2 - (nappula1.NappulanKoko / 2));
            }

            for (int i = 0; i < nappulatLista2.Count; i++)
            {
                nappula2 = nappulatLista2[i];
                OwnCanvas parent = ((Pelinappula)nappula2).Parent as OwnCanvas;
                nappula2.NappulanKoko = ((parent.ActualHeight / 2) * (parent.ActualWidth / 2)) * 0.025;
                if (nappula2.NappulanKoko > 80) nappula2.NappulanKoko = 80;
                Canvas.SetTop(nappula2, parent.ActualHeight / 2 - (nappula2.NappulanKoko / 2));
                Canvas.SetLeft(nappula2, parent.ActualWidth / 2 - (nappula2.NappulanKoko / 2));
            }

            if (kaikkiNappulat == true && (player1.Nappulat < 3 || player2.Nappulat < 3))
            {
                myCanvas.Children.Remove(myTextblock);
                pelilauta.Children.Remove(myCanvas);
                myStoryboard.Remove(this);
                voitti();
            }
        }
    }


    /// <summary>
    /// Oma canvas-luokka pelialueen muodostamiseen, joka lisää pari propertya avuksi. HalfWidth ja HalfHeight antavat puolitetun todellisen 
    /// leveyden ja korkeuden ja näihin voi bindata xamlista
    /// </summary>
    public class OwnCanvas : Canvas
    {
        /// <summary>
        /// Konstruktori
        /// </summary>
        public OwnCanvas()
        {
            HorizontalAlignment = HorizontalAlignment.Stretch;
            VerticalAlignment = VerticalAlignment.Stretch;
            SizeChanged += OwnCanvas_SizeChanged;
        }


        /// <summary>
        /// Laskee puolitetut arvo sekä pallon koon ja sijainnin uudelleen
        /// </summary>
        /// <param name="sender">OwnCanvas</param>
        /// <param name="e"></param>
        public void OwnCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            HalfWidth = ActualWidth / 2;
            HalfHeight = ActualHeight / 2;
            PalloKoko = (HalfHeight * HalfWidth) * 0.011;
            PalloSijainti = -(PalloKoko / 2);
        }


        /// <summary>
        /// Property puolikkaalle leveydelle
        /// </summary>
        public double HalfWidth
        {
            get { return (double)GetValue(HalfWidthProperty); }
            set { SetValue(HalfWidthProperty, value); }
        }

        public static readonly DependencyProperty HalfWidthProperty =
            DependencyProperty.Register("HalfWidth", typeof(double), typeof(OwnCanvas), new PropertyMetadata(0.0));


        /// <summary>
        /// Property puolikkaalle korkeudelle
        /// </summary>
        public double HalfHeight
        {
            get { return (double)GetValue(HalfHeightProperty); }
            set { SetValue(HalfHeightProperty, value); }
        }

        public static readonly DependencyProperty HalfHeightProperty =
            DependencyProperty.Register("HalfHeight", typeof(double), typeof(OwnCanvas), new PropertyMetadata(0.0));


        /// <summary>
        /// Property pallon koolle
        /// </summary>
        public double PalloKoko
        {
            get { return (double)GetValue(PalloKokoProperty); }
            set { SetValue(PalloKokoProperty, value); }
        }

        public static readonly DependencyProperty PalloKokoProperty =
            DependencyProperty.Register("PalloKoko", typeof(double), typeof(OwnCanvas), new PropertyMetadata(0.0));


        /// <summary>
        /// Property pallon sijainnille
        /// </summary>
        public double PalloSijainti
        {
            get { return (double)GetValue(PalloSijaintiProperty); }
            set { SetValue(PalloSijaintiProperty, value); }
        }

        public static readonly DependencyProperty PalloSijaintiProperty =
            DependencyProperty.Register("PalloSijainti", typeof(double), typeof(OwnCanvas), new PropertyMetadata(0.0));

    }

    public class Pelaaja
    {
        private string nimi;
        private int tyyppi;
        private int nappulat;


        /// <summary>
        /// Konstruktori
        /// </summary>
        public Pelaaja()
        {
            nimi = "Kalle";
            tyyppi = 0; // 0 ei ole tietokone, 1 on tietokone
        }


        /// <summary>
        /// Property pelaajan nimelle
        /// </summary>
        public string Nimi
        {
            get { return nimi; }
            set { nimi = value; }
        }


        /// <summary>
        /// Property pelaajan tyypille, 0 ei ole tietokone, 1 on tietokone. Mahdollista tietokoneen pelaamistoiminnon.
        /// </summary>
        public int Tyyppi
        {
            get { return tyyppi; }
            set { tyyppi = value; }
        }


        /// <summary>
        /// Pelaajan nappuloiden lkm
        /// </summary>
        public int Nappulat
        {
            get { return nappulat; }
            set { nappulat = value; }
        }
    }
}
