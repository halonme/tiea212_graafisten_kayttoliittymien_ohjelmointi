﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace PelinappulaControl
{


    /// <summary>
    /// Mylly -pelin pelinappula, joka osaa muuttaa muuttaa väriään ja muotoaan.
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class Pelinappula : UserControl
    {
        //Ellipse nappula;
        /// <summary>
        /// Property kertomaan pelinappulalle, onko nappula valittu. Estää monen nappulan valitsemisen.
        /// </summary>
        public bool Valittu
        {
            get { return valittu; }
            set { valittu = value; }
        }

        private bool valittu = false;


        /// <summary>
        /// Konstruktori
        /// </summary>
        public Pelinappula()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Property pelinappulan värille, jonka kauttaa nappula muuttaa värinsä
        /// </summary>
        public SolidColorBrush NappulanVari
        {
            get { return (SolidColorBrush)GetValue(NappulanVariProperty); }
            set { SetValue(NappulanVariProperty, value); }
        }

        public static readonly DependencyProperty NappulanVariProperty =
            DependencyProperty.Register("NappulanVari", typeof(SolidColorBrush), typeof(Pelinappula), new PropertyMetadata(new SolidColorBrush(Colors.Gray)));
          

        /// <summary>
        /// Property pelinappulan koolle. Osaa esim. pelialueen suurentuessa muuttaa kokoaan.
        /// </summary>
        public double NappulanKoko
        {
            get { return (double)GetValue(NappulanKokoProperty); }
            set { SetValue(NappulanKokoProperty, value); }
        }

        public static readonly DependencyProperty NappulanKokoProperty =
            DependencyProperty.Register("NappulanKoko", typeof(double), typeof(Pelinappula), new PropertyMetadata(35.0));


        /// <summary>
        /// Property pelinappulan muodolle. Osaa muuttaa muotoaan tämän kautta.
        /// </summary>
        public string NappulanMuoto
        {
            get { return (string)GetValue(NappulanMuotoProperty); }
            set { SetValue(NappulanMuotoProperty, value); }
        }

        public static readonly DependencyProperty NappulanMuotoProperty =
            DependencyProperty.Register("NappulanMuoto", typeof(string), typeof(Pelinappula), new PropertyMetadata("ellipse"));


        /// <summary>
        /// Pelialue kutsuu tätä, jos tietty pelinappula on valittu. Muuttaa valitun nappulan punaiseksi ja suuretaa sitä.
        /// </summary>
        public void valittuNappula()
        {
            if (Valittu == true)
            {
                //nappula = (Ellipse)sender;
                //nappula.Fill = new SolidColorBrush(Colors.Red);
                this.NappulanVari = new SolidColorBrush(Colors.Red);
                this.NappulanKoko = this.NappulanKoko * 1.10;
                //nappula.Fill = new SolidColorBrush(Colors.Red);
                //this.Height = 40;
                //this.Width = 40;
            }
            else return;
        }

    }


}
