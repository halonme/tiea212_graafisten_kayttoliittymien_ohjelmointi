﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Taso1
{
    /// <summary>
    /// Dialogi Mylly -pelin asetuksia varten. Asetetaan pelin värin sekä nappuloiden värit ja muodot, pelaajien nimet sekä kieli.
    /// Interaction logic for WindowSettings.xaml
    /// </summary>
    public partial class WindowSettings : Window
    {
        System.Windows.Forms.ColorDialog colorDialog;
        System.Drawing.Color muutosColor;


        /// <summary>
        /// Konstruktori
        /// </summary>
        public WindowSettings()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Property true, jos pelaaja 1 on tietokone
        /// </summary>
        public bool Computer1
        {
            get { return (bool)GetValue(Computer1Property); }
            set { SetValue(Computer1Property, value); }
        }

        public static readonly DependencyProperty Computer1Property =
        DependencyProperty.Register(
        "Computer1",
        typeof(bool), // propertyn tietotyyppi
        typeof(WindowSettings), // luokka jossa property sijaitsee
        new FrameworkPropertyMetadata(false,  // propertyn oletusarvo
        FrameworkPropertyMetadataOptions.AffectsRender, // vaikuttaa luokan ulkoasuun
        new PropertyChangedCallback(OnValueChangedComputer1),  // kutsutaan propertyn arvon muuttumisen jälkeen
        new CoerceValueCallback(MuutaComputer1))); // kutsutaan ennen propertyn arvon muutosta

        /// <summary>
        /// Kutsutaan ennen property arvon muuttamista.
        /// </summary>
        /// <param name="element">WindowSettings</param>
        /// <param name="value">propertyn arvo</param>
        /// <returns></returns>
        private static object MuutaComputer1(DependencyObject element, object value)
        {
            bool computer = (bool)value;
            return computer;
        }
    

        /// <summary>
        /// Propertyn arvon muuttamisen jälkeen. Tarkistaa, onko tietokone valittu. 
        /// </summary>
        /// <param name="obj">WindowSettings</param>
        /// <param name="args"></param>
        private static void OnValueChangedComputer1(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            WindowSettings o = (WindowSettings)obj;
            if (o.Computer1 == true) o.checkBox1.IsChecked = true;
            //else o.checkBox1.IsChecked = false;
        }


        /// <summary>
        /// Property true, jos pelaaja 2 on tietokone
        /// </summary>
        public bool Computer2
        {
            get { return (bool)GetValue(Computer2Property); }
            set { SetValue(Computer2Property, value); }
        }

        public static readonly DependencyProperty Computer2Property =
        DependencyProperty.Register(
        "Computer2",
        typeof(bool), // propertyn tietotyyppi
        typeof(WindowSettings), // luokka jossa property sijaitsee
        new FrameworkPropertyMetadata(false,  // propertyn oletusarvo
        FrameworkPropertyMetadataOptions.AffectsRender, // vaikuttaa luokan ulkoasuun
        new PropertyChangedCallback(OnValueChangedComputer2),  // kutsutaan propertyn arvon muuttumisen jälkeen
        new CoerceValueCallback(MuutaComputer2))); // kutsutaan ennen propertyn arvon muutosta

        /// <summary>
        /// Kutsutaan ennen property arvon muuttamista.
        /// </summary>
        /// <param name="element">WindowSettings</param>
        /// <param name="value">propertyn arvo</param>
        /// <returns></returns>
        private static object MuutaComputer2(DependencyObject element, object value)
        {
            bool computer = (bool)value;
            return computer;
        }


        /// <summary>
        /// Propertyn arvon muuttamisen jälkeen. Tarkistaa, onko tietokone valittu. 
        /// </summary>
        /// <param name="obj">WindowSettings</param>
        /// <param name="args"></param>
        private static void OnValueChangedComputer2(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            WindowSettings o = (WindowSettings)obj;
            if (o.Computer2 == true) o.checkBox2.IsChecked = true;
            //else o.checkBox2.IsChecked = false;
        }


        /// <summary>
        /// Property true, jos käyttäjä valinnut kieleksi englanti
        /// </summary>
        public bool English
        {
            get { return (bool)GetValue(EnglishProperty); }
            set { SetValue(EnglishProperty, value); }
        }

        public static readonly DependencyProperty EnglishProperty =
        DependencyProperty.Register(
        "English",
        typeof(bool), // propertyn tietotyyppi
        typeof(WindowSettings), // luokka jossa property sijaitsee
        new FrameworkPropertyMetadata(false,  // propertyn oletusarvo
        FrameworkPropertyMetadataOptions.AffectsRender, // vaikuttaa luokan ulkoasuun
        new PropertyChangedCallback(OnValueChangedEnglish),  // kutsutaan propertyn arvon muuttumisen jälkeen
        new CoerceValueCallback(MuutaEnglish))); // kutsutaan ennen propertyn arvon muutosta

        /// <summary>
        /// Kutsutaan ennen property arvon muuttamista. 
        /// </summary>
        /// <param name="element">WindowSettings</param>
        /// <param name="value">propertyn arvo</param>
        /// <returns></returns>
        private static object MuutaEnglish(DependencyObject element, object value)
        {
            bool enkku = (bool)value;
            return enkku;
        }


        /// <summary>
        /// Propertyn arvon muuttamisen jälkeen. Tarkistaa on englanti valittu
        /// </summary>
        /// <param name="obj">WindowSettings</param>
        /// <param name="args"></param>
        private static void OnValueChangedEnglish(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            WindowSettings o = (WindowSettings)obj;
            if (o.English == true) o.radioButtonEn.IsChecked = true;
            //else o.radioButtonEn.IsChecked = false;
        }


        /// <summary>
        /// Property true, jos käyttäjä valinnut kieleksi suomen
        /// </summary>
        public bool Finnish
        {
            get { return (bool)GetValue(FinnishProperty); }
            set { SetValue(FinnishProperty, value); }
        }

        public static readonly DependencyProperty FinnishProperty =
        DependencyProperty.Register(
        "Finnish",
        typeof(bool), // propertyn tietotyyppi
        typeof(WindowSettings), // luokka jossa property sijaitsee
        new FrameworkPropertyMetadata(false,  // propertyn oletusarvo
        FrameworkPropertyMetadataOptions.AffectsRender, // vaikuttaa luokan ulkoasuun
        new PropertyChangedCallback(OnValueChangedFinnish),  // kutsutaan propertyn arvon muuttumisen jälkeen
        new CoerceValueCallback(MuutaFinnish))); // kutsutaan ennen propertyn arvon muutosta

        /// <summary>
        /// Kutsutaan ennen property arvon muuttamista. 
        /// </summary>
        /// <param name="element">WindowSettings</param>
        /// <param name="value">propertyn arvo</param>
        /// <returns></returns>
        private static object MuutaFinnish(DependencyObject element, object value)
        {
            bool suomi = (bool)value;
            return suomi;
        }


        /// <summary>
        /// Propertyn arvon muuttamisen jälkeen. Tarkistaa, onko suomi valittu kieleksi.
        /// </summary>
        /// <param name="obj">WindowSettings</param>
        /// <param name="args"></param>
        private static void OnValueChangedFinnish(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            WindowSettings o = (WindowSettings)obj;
            if (o.Finnish == true) o.radioButtonFin.IsChecked = true;
            //else o.radioButtonEn.IsChecked = false;
        }


        /// <summary>
        /// Property pelilaudan värille
        /// </summary>
        public SolidColorBrush PelialueVari
        {
            get { return (SolidColorBrush)GetValue(PelialueVariProperty); }
            set { SetValue(PelialueVariProperty, value); }
        }

        public static readonly DependencyProperty PelialueVariProperty =
            DependencyProperty.Register("PelialueVari", typeof(SolidColorBrush), typeof(WindowSettings), new PropertyMetadata(new SolidColorBrush(Colors.White)));

        /// <summary>
        /// Property pelaaja 1 nappulan värille.
        /// </summary>
        public SolidColorBrush NappulanVari1
        {
            get { return (SolidColorBrush)GetValue(NappulanVari1Property); }
            set { SetValue(NappulanVari1Property, value); }
        }

        public static readonly DependencyProperty NappulanVari1Property =
            DependencyProperty.Register("NappulanVari1", typeof(SolidColorBrush), typeof(WindowSettings), new PropertyMetadata(new SolidColorBrush(Colors.Gray)));


        /// <summary>
        /// Property pelaaja 2 nappulan värille.
        /// </summary>
        public SolidColorBrush NappulanVari2
        {
            get { return (SolidColorBrush)GetValue(NappulanVari2Property); }
            set { SetValue(NappulanVari2Property, value); }
        }

        public static readonly DependencyProperty NappulanVari2Property =
            DependencyProperty.Register("NappulanVari2", typeof(SolidColorBrush), typeof(WindowSettings), new PropertyMetadata(new SolidColorBrush(Colors.Gray)));


        /// <summary>
        /// Property pelaajan 1 nimelle. Jos nimeä ei syötetä käytetään oletusnimeä.
        /// </summary>
        public static readonly DependencyProperty Pelaaja1NameProperty =
        DependencyProperty.Register(
        "Pelaaja1Name",
        typeof(string), // propertyn tietotyyppi
        typeof(WindowSettings), // luokka jossa property sijaitsee
        new FrameworkPropertyMetadata("Maija",  // propertyn oletusarvo
        FrameworkPropertyMetadataOptions.AffectsRender, // vaikuttaa luokan ulkoasuun 
        new PropertyChangedCallback(OnValueChanged),  // kutsutaan propertyn arvon muuttumisen jälkeen
        new CoerceValueCallback(MuutaNimi))); // kutsutaan ennen propertyn arvon muutosta

        public string Pelaaja1Name
        {
            get { return (string)GetValue(Pelaaja1NameProperty); }
            set { SetValue(Pelaaja1NameProperty, value); }
        }

        /// <summary>
        /// Kutsutaan ennen property arvon muuttamista. Tarkistaa, jos nimeä ei ole, niin laittaa oletusnimen.
        /// </summary>
        /// <param name="element">WindowSettings</param>
        /// <param name="value">propertyn arvo</param>
        /// <returns></returns>
        private static object MuutaNimi(DependencyObject element, object value)
        {
            string nimi = (string)value;
            string trimattunimi = nimi.Trim();
            if (trimattunimi == "") value = "Maija";

            return trimattunimi;
        }

        /// <summary>
        /// Propertyn arvon muuttamisen jälkeen. Jos nimeä ei annettu laittaa oletusnimen. 
        /// </summary>
        /// <param name="obj">WindowSettings</param>
        /// <param name="args">tapahtuma</param>
        private static void OnValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            WindowSettings o = (WindowSettings)obj;
            if (o.Pelaaja1Name == "") o.Pelaaja1Name = "Maija";
        }


        /// <summary>
        /// Property pelaaja 2 nimelle.
        /// </summary>
        public static readonly DependencyProperty Pelaaja2NameProperty =
        DependencyProperty.Register(
        "Pelaaja2Name",
        typeof(string), // propertyn tietotyyppi
        typeof(WindowSettings), // luokka jossa property sijaitsee
        new FrameworkPropertyMetadata("Matti",  // propertyn oletusarvo
        FrameworkPropertyMetadataOptions.AffectsRender, // vaikuttaa luokan ulkoasuun
        new PropertyChangedCallback(OnValueChanged2),  // kutsutaan propertyn arvon muuttumisen jälkeen
        new CoerceValueCallback(MuutaNimi2))); // kutsutaan ennen propertyn arvon muutosta

        public string Pelaaja2Name
        {
            get { return (string)GetValue(Pelaaja2NameProperty); }
            set { SetValue(Pelaaja2NameProperty, value); }
        }


        /// <summary>
        /// Kutsutaan ennen property arvon muuttamista. Tarkistaa, jos nimeä ei ole, niin laittaa oletusnimen.
        /// </summary>
        /// <param name="element">WindowSettings</param>
        /// <param name="value">propertyn arvo</param>
        /// <returns></returns>
        private static object MuutaNimi2(DependencyObject element, object value)
        {
            string nimi = (string)value;
            string trimattunimi = nimi.Trim();

            if (trimattunimi == "") value = "Matti";

            return trimattunimi;
        }


        /// <summary>
        /// Propertyn arvon muuttamisen jälkeen. Jos nimeä ei annettu laittaa oletusnimen. 
        /// </summary>
        /// <param name="obj">WindowSettings</param>
        /// <param name="args"></param>
        private static void OnValueChanged2(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            WindowSettings o = (WindowSettings)obj;
            if (o.Pelaaja2Name == "") o.Pelaaja2Name = "Matti";
        }


        /// <summary>
        /// Property pelaaja 1 nappulan muodolle
        /// </summary>
        public string nappulanMuoto1
        {
            get { return (string)GetValue(nappulanMuoto1Property); }
            set { SetValue(nappulanMuoto1Property, value); }
        }

        public static readonly DependencyProperty nappulanMuoto1Property =
        DependencyProperty.Register(
        "nappulanMuoto1",
        typeof(string), // propertyn tietotyyppi
        typeof(WindowSettings), // luokka jossa property sijaitsee
        new FrameworkPropertyMetadata("ellipse",  // propertyn oletusarvo
        FrameworkPropertyMetadataOptions.AffectsRender, // vaikuttaa luokan ulkoasuun
        new PropertyChangedCallback(OnValueChangedMuoto1),  // kutsutaan propertyn arvon muuttumisen jälkeen
        new CoerceValueCallback(MuutaMuoto1))); // kutsutaan ennen propertyn arvon muutosta

        /// <summary>
        /// Kutsutaan ennen property arvon muuttamista. 
        /// </summary>
        /// <param name="element">WindowSettings</param>
        /// <param name="value">propertyn arvo</param>
        /// <returns></returns>
        private static object MuutaMuoto1(DependencyObject element, object value)
        {
            string muoto = (string)value;
            return muoto;
        }


        /// <summary>
        /// Propertyn arvon muuttamisen jälkeen. Näyttää nykyisen valitun muodon, kun settings -dialogi käynnistetään.
        /// </summary>
        /// <param name="obj">WindowSettings</param>
        /// <param name="args"></param>
        private static void OnValueChangedMuoto1(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            WindowSettings o = (WindowSettings)obj;
            if (o.nappulanMuoto1.Equals("ellipse")) o.ympyra1.IsChecked = true;
            if (o.nappulanMuoto1.Equals("nelio")) o.nelio1.IsChecked = true;
            else if (o.nappulanMuoto1.Equals("kolmio")) o.kolmio1.IsChecked = true;
        }


        /// <summary>
        /// Property pelaaja 2 nappulan muodolle
        /// </summary>
        public string nappulanMuoto2
        {
            get { return (string)GetValue(nappulanMuoto2Property); }
            set { SetValue(nappulanMuoto2Property, value); }
        }

        public static readonly DependencyProperty nappulanMuoto2Property =
        DependencyProperty.Register(
        "nappulanMuoto2",
        typeof(string), // propertyn tietotyyppi
        typeof(WindowSettings), // luokka jossa property sijaitsee
        new FrameworkPropertyMetadata("ellipse",  // propertyn oletusarvo
        FrameworkPropertyMetadataOptions.AffectsRender, // vaikuttaa luokan ulkoasuun
        new PropertyChangedCallback(OnValueChangedMuoto2),  // kutsutaan propertyn arvon muuttumisen jälkeen
        new CoerceValueCallback(MuutaMuoto2))); // kutsutaan ennen propertyn arvon muutosta

        /// <summary>
        /// Kutsutaan ennen property arvon muuttamista.
        /// </summary>
        /// <param name="element">WindowSettings</param>
        /// <param name="value">propertyn arvo</param>
        /// <returns></returns>
        private static object MuutaMuoto2(DependencyObject element, object value)
        {
            string muoto2 = (string)value;
            return muoto2;
        }


        /// <summary>
        /// Propertyn arvon muuttamisen jälkeen. Näyttää nykyisen valitun muodon, kun settings -dialogi käynnistetään.
        /// </summary>
        /// <param name="obj">WindowSettings</param>
        /// <param name="args"></param>
        private static void OnValueChangedMuoto2(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            WindowSettings o2 = (WindowSettings)obj;
            if (o2.nappulanMuoto2.Equals("ellipse")) o2.ympyra2.IsChecked = true;
            if (o2.nappulanMuoto2.Equals("nelio")) o2.nelio2.IsChecked = true;
            else if (o2.nappulanMuoto2.Equals("kolmio")) o2.kolmio2.IsChecked = true;
        }


        /// <summary>
        /// Tapahtuu, kun Settingsin pelilaudan väri -painikke painettu.
        /// Color dialogin avulla alustaa siihen nykyisen värin ja ottaa propertyyn talteen valitun värin.
        /// </summary>
        /// <param name="sender">Choose Color -painike</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void btnTaustaVari_Click(object sender, RoutedEventArgs e)
        {
            Color vari = colorPicker(PelialueVari);
            PelialueVari = new SolidColorBrush(vari);
        }


        /// <summary>
        /// Tapahtuu, kun Settingsin pelaaja 1 väri -painikke painettu.
        /// Color dialogin avulla alustaa siihen nykyisen värin ja ottaa propertyyn talteen valitun värin.
        /// </summary>
        /// <param name="sender">Choose Color -painike</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void btnNappulanVari1_Click(object sender, RoutedEventArgs e)
        {
            Color vari = colorPicker(NappulanVari1);
            NappulanVari1 = new SolidColorBrush(vari);
        }


        /// <summary>
        /// Tapahtuu, kun Settingsin pelaaja 2 väri -painikke painettu.
        /// Color dialogin avulla alustaa siihen nykyisen värin ja ottaa propertyyn talteen valitun värin.
        /// </summary>
        /// <param name="sender">Choose Color -painike</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void btnNappulanVari2_Click(object sender, RoutedEventArgs e)
        {
            Color vari = colorPicker(NappulanVari2);
            NappulanVari2 = new SolidColorBrush(vari);
        }


        /// <summary>
        /// Luo Color dialogin ja alustaa siihen nykyisen värin sekä ottaa värin käyttäjältä
        /// </summary>
        /// <param name="variProperty">Property, mihin väri sijoitetaan</param>
        /// <returns>Palauttaa valitun värin</returns>
        private System.Windows.Media.Color colorPicker(SolidColorBrush variProperty)
        {
            colorDialog = new System.Windows.Forms.ColorDialog();
            muutosColor = System.Drawing.Color.FromArgb(variProperty.Color.A,
                                     variProperty.Color.R,
                                     variProperty.Color.G,
                                     variProperty.Color.B);
            colorDialog.Color = muutosColor;

            colorDialog.AllowFullOpen = true;
            colorDialog.ShowDialog();
            Color wpfColor = Color.FromArgb(colorDialog.Color.A, colorDialog.Color.R, colorDialog.Color.G, colorDialog.Color.B);

            return wpfColor;
        }


        /// <summary>
        /// Tapahtuu, kun dialogin OK -painiketta painettu. Asettaa dialogin tulokseksi True.
        /// </summary>
        /// <param name="sender">OK -painike</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void btnDialogOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }


        /// <summary>
        /// Tapahtuu, kun dialogin Cancel -painiketta painettu. Asetaa dialogin tulokseksi False.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDialogCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }


        /// <summary>
        /// Tapahtuu, kun pelaaja 1 radiobuttonista on valittu pelinappulan muodoksi ellipse tai alustetaan
        /// settings viimeksi valitulla.
        /// </summary>
        /// <param name="sender">Radiobutton ellipse, IsChecked</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void ympyra1_Checked(object sender, RoutedEventArgs e)
        {
            nappulanMuoto1 = "ellipse";
        }


        /// <summary>
        /// Tapahtuu, kun pelaaja 1 radiobuttonista on valittu pelinappulan muodoksi square tai alustetaan
        /// settings viimeksi valitulla.
        /// </summary>
        /// <param name="sender">Radiobutton square, IsChecked</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void nelio1_Checked(object sender, RoutedEventArgs e)
        {
            nappulanMuoto1 = "nelio";
        }


        /// <summary>
        /// Tapahtuu, kun pelaaja 1 radiobuttonista on valittu pelinappulan muodoksi triangle tai alustetaan
        /// settings viimeksi valitulla.
        /// </summary>
        /// <param name="sender">Radiobutton triangle, IsChecked</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void kolmio1_Checked(object sender, RoutedEventArgs e)
        {
            nappulanMuoto1 = "kolmio";
        }


        /// <summary>
        /// Tapahtuu, kun pelaaja 2 radiobuttonista on valittu pelinappulan muodoksi ellipse tai alustetaan
        /// settings viimeksi valitulla.
        /// </summary>
        /// <param name="sender">Radiobutton ellipse, IsChecked</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void ympyra2_Checked(object sender, RoutedEventArgs e)
        {
            nappulanMuoto2 = "ellipse";
        }


        /// <summary>
        /// Tapahtuu, kun pelaaja 2 radiobuttonista on valittu pelinappulan muodoksi square tai alustetaan
        /// settings viimeksi valitulla.
        /// </summary>
        /// <param name="sender">Radiobutton square, IsChecked</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void nelio2_Checked(object sender, RoutedEventArgs e)
        {
            nappulanMuoto2 = "nelio";
        }


        /// <summary>
        /// Tapahtuu, kun pelaaja 2 radiobuttonista on valittu pelinappulan muodoksi triangle tai alustetaan
        /// settings viimeksi valitulla.
        /// </summary>
        /// <param name="sender">Radiobutton triangle, IsChecked</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void kolmio2_Checked(object sender, RoutedEventArgs e)
        {
            nappulanMuoto2 = "kolmio";
        }


        /// <summary>
        /// Tapahtuu, kun pelaaja 1 kohdalta on valittu computer.
        /// </summary>
        /// <param name="sender">checbox</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void checkBox1_Checked(object sender, RoutedEventArgs e)
        {
            Computer1 = true;
        }


        /// <summary>
        /// Tapahtuu, kun pelaaja 2 kohdalta on valittu computer
        /// </summary>
        /// <param name="sender">checkbox</param>
        /// <param name="e">taphatuma, joka tapahtuu</param>
        private void checkBox2_Checked(object sender, RoutedEventArgs e)
        {
            Computer2 = true;
        }


        /// <summary>
        /// Tapahtuu, kun otetaan pelaaja 1 kohdalta computer valinta pois
        /// </summary>
        /// <param name="sender">checkbox</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void checkBox1_Unchecked(object sender, RoutedEventArgs e)
        {
            Computer1 = false;
        }


        /// <summary>
        /// Tapahtuu, kun otetaan pelaaja 2 kohdalta computer valinta pois
        /// </summary>
        /// <param name="sender">checkbox</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void checkBox2_Unchecked(object sender, RoutedEventArgs e)
        {
            Computer2 = false;
        }


        /// <summary>
        /// Tapahtuu, kun valitaan englanti kieleksi
        /// </summary>
        /// <param name="sender">radiobutton</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void radioButtonEn_Checked(object sender, RoutedEventArgs e)
        {
            English = true;
        }


        /// <summary>
        /// Tapahtuu, kun valitaan kieleksi suomi
        /// </summary>
        /// <param name="sender">radiobutton</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void radioButtonFin_Checked(object sender, RoutedEventArgs e)
        {
            Finnish = true;
        }


        /// <summary>
        /// Tapahtuu, kun otetaan kielivalinnasta englanti pois
        /// </summary>
        /// <param name="sender">radiiobutton</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void radioButtonEn_Unchecked(object sender, RoutedEventArgs e)
        {
            English = false;
        }


        /// <summary>
        /// Tapahtuu, kun otetaan kielivalinnasta suomi pois.
        /// </summary>
        /// <param name="sender">radiobutton</param>
        /// <param name="e">tapahtuma, joka tapahtuu</param>
        private void radioButtonFin_Unchecked(object sender, RoutedEventArgs e)
        {
            Finnish = false;
        }


        /// <summary>
        /// Oma Command textboxien nimien tarkistukseen ja OK -painikkeen aktivoimiseen.
        /// </summary>
        /// <param name="target">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        void SyottoCmdExecuted(object target, ExecutedRoutedEventArgs e)
        {
            String command, targetobj;
            command = ((RoutedCommand)e.Command).Name;
            targetobj = ((FrameworkElement)target).Name;

        }


        /// <summary>
        /// Tarkistaa, onko nimi syötetty oikeassa muodossa. Jos ei ole, niin OK painike ei ole aktivoituna.
        /// </summary>
        /// <param name="sender">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        void SyottoCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IsValid(sender as DependencyObject);
        }


        /// <summary>
        /// Tarkistaa, onko kaikki textboxien syöttö oikeanlainen.
        /// </summary>
        /// <param name="obj">WindowSettings</param>
        /// <returns></returns>
        private bool IsValid(DependencyObject obj)
        {
            return !Validation.GetHasError(obj) &&
            LogicalTreeHelper.GetChildren(obj)
            .OfType<DependencyObject>()
            .All(IsValid);

        }

    }


    /// <summary>
    /// Luokka oman validatiosääntöä varten. Tarkistaa, onko syötetty aika oikealla välillä.
    /// </summary>
    class SyotonTarkistusRule : ValidationRule
    {

        /// <summary>
        /// Konstruktori
        /// </summary>
        public SyotonTarkistusRule()
        {
        }


        /// <summary>
        /// Tarkistaa, onko syötetty nimi oikeanlainen. Ei kelpuuta erikoismerkkejä.       
        /// </summary>
        /// <param name="value">tarkistettava nimi</param>
        /// <param name="cultureInfo">kulttuuri-info muotoiluun</param>
        /// <returns></returns>
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            string teksti = ((string)value);
            string trimattuteksti = teksti.Trim();

            if (Regex.IsMatch(trimattuteksti, @"[^a-zA-Z0-9-]")) return new ValidationResult(false, "Not special characters");
            else return ValidationResult.ValidResult;
        }
    }


    /// <summary>
    /// Luokka omaa Syotto-Command määrittelyä varten
    /// </summary>
    public static class SyottoCommands
    {
        public static readonly RoutedUICommand Syotto = new RoutedUICommand
                (
                        "Syotto",
                        "Syotto",
                        typeof(SyottoCommands)
                );
    }
}
