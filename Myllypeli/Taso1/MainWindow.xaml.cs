﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PelialueControl;
using System.Windows.Markup;
using System.Globalization;


namespace Taso1
{
    /// <summary>
    /// Mylly -peli, jossa kaksi pelaajaa pelaa vastakkain, yrittäen saada mylly, jotta saa poistettua toisen nappuloita.
    /// MainWindoe hoitaa tietojen siirtelyn settingsien ja pelialueen välillä. Hoitaa kaikki pelin tiedottamisen
    /// pelaajille.
    /// 31.8.2017
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        WindowSettings dialog;

        /// <summary>
        /// Konstruktori. Muuttaa käyttäjän asettaman kielen.
        /// </summary>
        public MainWindow()
        {
            Eng = Properties.Settings.Default.Eng;
            Fin = Properties.Settings.Default.Fin;
            if (Eng == true) Taso1.Properties.Resources.Culture = new CultureInfo("en-US");
            else if (Eng == false) Taso1.Properties.Resources.Culture = new CultureInfo("fi-FI");


            InitializeComponent();
            this.DataContext = dialog;
            peli.Enkku = Eng;
            peli.Suomi = Fin;
        }


        /// <summary>
        /// Property true, jos kieli englanti
        /// </summary>
        public bool Eng
        {
            get { return eng; }
            set { eng = value; }
        }

        private bool eng = true;


        /// <summary>
        /// Property true, jos kieli suomi
        /// </summary>
        public bool Fin
        {
            get { return fin; }
            set { fin = value; }
        }

        private bool fin = false;


        /// <summary>
        /// Property pelilaudan taustavärille. MainWindow ottaa tähän talteen Settingsien pelilaudan värin ja muuttaa
        /// tätä kautta pelilaudan taustavärin.
        /// </summary>
        public SolidColorBrush PeliVari
        {
            get { return (SolidColorBrush)GetValue(PeliVariProperty); }
            set { SetValue(PeliVariProperty, value); }
        }

        public static readonly DependencyProperty PeliVariProperty =
            DependencyProperty.Register("PeliVari", typeof(SolidColorBrush), typeof(WindowSettings), new PropertyMetadata(new SolidColorBrush(Colors.White)));


        /// <summary>
        /// Oma Command Menun New Game valintaan. Kutsuu Pelialueen TyhjennaPelilautaa, uutta peliä varten
        /// </summary>
        /// <param name="target">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        void NewGameCmdExecuted(object target, ExecutedRoutedEventArgs e)
        {
            String command, targetobj;
            command = ((RoutedCommand)e.Command).Name;
            targetobj = ((FrameworkElement)target).Name;

            peli.TyhjennaPelilauta();
        }


        /// <summary>
        /// Tarkistaa, voidaanko uusi peli aloittaa. Asetettu aina suoritettavaksi.
        /// </summary>
        /// <param name="sender">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        void NewGameCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }


        /// <summary>
        /// Oma Command Menun About -dialogin näyttämiseen.
        /// Käyttää NewGameCommandCanExecute tarkistamiseen eli voi suorittaa aina.
        /// </summary>
        /// <param name="target">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        void AboutExecuted(object target, ExecutedRoutedEventArgs e)
        {
            String command, targetobj;
            command = ((RoutedCommand)e.Command).Name;
            targetobj = ((FrameworkElement)target).Name;

            AboutBox1 aboutDialog = new AboutBox1();
            aboutDialog.ShowDialog();

        }


        /// <summary>
        /// Oma Command Setting -dialogin näyttämiseen.
        /// Alustaa settings -dialogin asetukset (värit ja muodot) ennen dialogin avaamista.
        /// Jos dialogissa valitty Ok, niin vaihtaa värit, nimet ja muodot.
        /// Jos jonkun pelaajan nappulan väri on sama kuin pelilaudan väri, niin ei muuta pelilaudan väriä.
        /// Käyttää NewGameCommandCanExecute tarkistamiseen eli voi suorittaa aina.
        /// </summary>
        /// <param name="target">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        void SettingsCmdExecuted(object target, ExecutedRoutedEventArgs e)
        {
            String command, targetobj;
            command = ((RoutedCommand)e.Command).Name;
            targetobj = ((FrameworkElement)target).Name;

            dialog = new WindowSettings();
            dialog.Owner = this;

            dialog.PelialueVari = PeliVari;
            dialog.NappulanVari1 = peli.VariNappulat1;
            dialog.NappulanVari2 = peli.VariNappulat2;
            dialog.nappulanMuoto1 = peli.MuotoNappula1;
            dialog.nappulanMuoto2 = peli.MuotoNappula2;
            dialog.Computer1 = peli.Tietokone1;
            dialog.Computer2 = peli.Tietokone2;
            dialog.English = Eng;
            dialog.Finnish = Fin;

            //Aukaisee dialogin
            dialog.ShowDialog();

            // Tapahtuu, jos dialogista valittu OK
            if (dialog.DialogResult == true)
            {
                peli.Player1Name = dialog.Pelaaja1Name;
                peli.Player2Name = dialog.Pelaaja2Name;
                if (peli.PelaajanVuoro == 1) peli.VuorossaPelaaja = peli.Player1Name;
                else peli.VuorossaPelaaja = peli.Player2Name;

                peli.VariNappulat1 = dialog.NappulanVari1;
                peli.VariNappulat2 = dialog.NappulanVari2;
                if (!dialog.PelialueVari.Color.Equals(dialog.NappulanVari1.Color) && !dialog.PelialueVari.Color.Equals(dialog.NappulanVari2.Color))
                {
                    peli.Background = dialog.PelialueVari;
                    PeliVari = dialog.PelialueVari;
                }

                peli.MuotoNappula1 = dialog.nappulanMuoto1;
                peli.MuotoNappula2 = dialog.nappulanMuoto2;

                peli.Tietokone1 = dialog.Computer1;
                peli.Tietokone2 = dialog.Computer2;
                Eng = dialog.English;
                Fin = dialog.Finnish;
            }
        }


        /// <summary>
        /// Application command Print. Käytössä, jos pelilaudalla on vähintää yksi nappula.
        /// </summary>
        /// <param name="sender">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        private void PrintCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (peli.Nappuloita > 0) e.CanExecute = true;
        }


        /// <summary>
        /// Application command Print. Tulostaa nykyisen pelilaudan ja nappulat ja Mylly -tekstin yhdelle sivulle. 
        /// Käyttää NewGameCommandCanExecute tarkistamiseen eli voi suorittaa aina.
        /// </summary>
        /// <param name="sender">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        private void PrintCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PrintDialog dialog = new PrintDialog();
            if (dialog.ShowDialog() == true)
            {
                FixedDocument document = new FixedDocument();
                document.DocumentPaginator.PageSize = new Size(dialog.PrintableAreaWidth - 40, dialog.PrintableAreaHeight - 40);

                FixedPage page1 = new FixedPage();
                page1.Width = document.DocumentPaginator.PageSize.Width;
                page1.Height = document.DocumentPaginator.PageSize.Height;

                DockPanel panel = new DockPanel();
                panel.Width = page1.Width;
                panel.Height = page1.Height;
                page1.Children.Add(panel);
                panel.LastChildFill = true;

                TextBlock header = new TextBlock();
                header.Text = "Mylly";
                header.FontSize = 60;

                header.Margin = new Thickness(290, 0, 0, 20);
                panel.Children.Add(header);
                DockPanel.SetDock(header, Dock.Top);

                Grid parent = ((Pelialue)peli).Parent as Grid;
                parent.Children.Remove(peli);
                peli.Margin = new Thickness(20, 20, 20, 20);
                panel.Children.Add(peli);
                DockPanel.SetDock(peli, Dock.Bottom);

                PageContent page1Content = new PageContent();
                ((IAddChild)page1Content).AddChild(page1);
                document.Pages.Add(page1Content);

                dialog.PrintDocument(document.DocumentPaginator, "Fixed document");

                panel.Children.Remove(peli);
                parent.Children.Add(peli);
            }
        }


        /// <summary>
        /// Application command Help. Näyttää Wikipedian sivun, jossa on pelin ohjeet.
        /// Käyttää NewGameCommandCanExecute tarkistamiseen eli voi suorittaa aina.
        /// </summary>
        /// <param name="sender">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        private void HelpCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://fi.wikipedia.org/wiki/Mylly_%28peli%29");
        }


        /// <summary>
        /// Application command Close. Sulkee ohjelman.
        /// Käyttää NewGameCommandCanExecute tarkistamiseen eli voi suorittaa aina.
        /// </summary>
        /// <param name="sender">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        private void CloseCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }


        /// <summary>
        /// Tapahtuu, kun ikkuna suljetaan. Tallentaa kaikki asetukset eli värit, nimet ja muodot.
        /// </summary>
        /// <param name="sender">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        private void ikkuna_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Player1Name = peli.Player1Name;
            Properties.Settings.Default.Player2Name = peli.Player2Name;
            Properties.Settings.Default.PelilautaVari = System.Drawing.Color.FromArgb(PeliVari.Color.A,
                                     PeliVari.Color.R,
                                     PeliVari.Color.G,
                                     PeliVari.Color.B);
            Properties.Settings.Default.Player1Vari = System.Drawing.Color.FromArgb(peli.VariNappulat1.Color.A,
                         peli.VariNappulat1.Color.R,
                         peli.VariNappulat1.Color.G,
                         peli.VariNappulat1.Color.B);
            Properties.Settings.Default.Player2Vari = System.Drawing.Color.FromArgb(peli.VariNappulat2.Color.A,
             peli.VariNappulat2.Color.R,
             peli.VariNappulat2.Color.G,
             peli.VariNappulat2.Color.B);

            Properties.Settings.Default.Muoto1 = peli.MuotoNappula1;
            Properties.Settings.Default.Muoto2 = peli.MuotoNappula2;
            Properties.Settings.Default.Computer1 = peli.Tietokone1;
            Properties.Settings.Default.Computer2 = peli.Tietokone2;
            Properties.Settings.Default.Eng = Eng;
            Properties.Settings.Default.Fin = Fin;


            Properties.Settings.Default.Save();
        }


        /// <summary>
        /// Tapahtuu, kun ohjelma käynnistetään. Alustaa nimet, värit ja muodot tallennetuilla asetuksilla.
        /// </summary>
        /// <param name="sender">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        private void ikkuna_Loaded(object sender, RoutedEventArgs e)
        {
            peli.Player1Name = Properties.Settings.Default.Player1Name;
            peli.Player2Name = Properties.Settings.Default.Player2Name;
            PeliVari = new SolidColorBrush(Color.FromArgb(Properties.Settings.Default.PelilautaVari.A, Properties.Settings.Default.PelilautaVari.R, Properties.Settings.Default.PelilautaVari.G, Properties.Settings.Default.PelilautaVari.B));
            peli.Background = new SolidColorBrush(Color.FromArgb(Properties.Settings.Default.PelilautaVari.A, Properties.Settings.Default.PelilautaVari.R, Properties.Settings.Default.PelilautaVari.G, Properties.Settings.Default.PelilautaVari.B));
            peli.VariNappulat1 = new SolidColorBrush(Color.FromArgb(Properties.Settings.Default.Player1Vari.A, Properties.Settings.Default.Player1Vari.R, Properties.Settings.Default.Player1Vari.G, Properties.Settings.Default.Player1Vari.B));
            peli.VariNappulat2 = new SolidColorBrush(Color.FromArgb(Properties.Settings.Default.Player2Vari.A, Properties.Settings.Default.Player2Vari.R, Properties.Settings.Default.Player2Vari.G, Properties.Settings.Default.Player2Vari.B));
            peli.MuotoNappula1 = Properties.Settings.Default.Muoto1;
            peli.MuotoNappula2 = Properties.Settings.Default.Muoto2;
            peli.Tietokone1 = Properties.Settings.Default.Computer1;
            peli.Tietokone2 = Properties.Settings.Default.Computer2;
            Eng = Properties.Settings.Default.Eng;
            Fin = Properties.Settings.Default.Fin;

            peli.arvoVuoro();
        }
    }


    /// <summary>
    /// Luokka omaa New Game-Command määrittelyä varten
    /// </summary>
    public static class NewGameCommands
    {
        public static readonly RoutedUICommand NewGame = new RoutedUICommand
                (
                        "NewGame",
                        "NewGame",
                        typeof(NewGameCommands)
                );
    }


    /// <summary>
    /// Luokka omaa Settings-Command määrittelyä varten
    /// </summary>
    public static class SettingsCommands
    {
        public static readonly RoutedUICommand Settings = new RoutedUICommand
                (
                        "Settings",
                        "Settings",
                        typeof(SettingsCommands)
                );
    }


    /// <summary>
    /// Luokka omaa About-Command määrittelyä varten
    /// </summary>
    public static class AboutCommands
    {
        public static readonly RoutedUICommand About = new RoutedUICommand
                (
                        "About",
                        "About",
                        typeof(AboutCommands)
                );
    }
}
