﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.ComponentModel;
using System.Timers;
using System.Diagnostics;

namespace Taso3
{
    /// <summary>
    /// 28.7.2017 Merja Halonen, Siirtelypeli
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static RoutedCommand UusiPeliCommand = new RoutedCommand();

        //laskee montako raahattu dockpaneliin
        private int montako = 0;
        //laskee viimeisen paikan indeksin
        private int viimeinen = -1;
        private double left;
        private double top;


        List<RaahattuLabel> raahatutLabelit = new List<RaahattuLabel>();
        List<string> funktioLuvut = new List<string>();

        Point startPoint = new Point(0, 0);

        Point point = new Point(0, 0);
        double X = 0;
        int indeksi = 0;

        RaahattuLabel uusiLabel;
        RaahattuLabel tyhjaLabel;
        TextBox tekstikentta = new TextBox();

        Random rand = new Random();

        Stopwatch aikaLaskuri = new Stopwatch();



        /// <summary>
        /// Konstruktori
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            CommandBinding OmaCommandBinding = new CommandBinding(
            UusiPeliCommand, UusiPeliExecuted, UusiPeliCanExecute);

            CommandBindings.Add(OmaCommandBinding);
            buttonLisaa.Command = UusiPeliCommand;

        }


        /// <summary>
        /// Aliohjelma, jota kutsutaan uuden pelin alussa. Tyhjentää tiedot vanhasta pelistä.
        /// </summary>
        public void tyhjennaPeli()
        {
            Lukualue.Children.Clear();
            Tulosalue.Children.Clear();
            funktioLuvut.Clear();
            raahatutLabelit.Clear();
            montako = 0;
            Tulosalue.Background = Brushes.Transparent;
            Oikein.Content = "";
            viimeinen = -1;
            Laskuri = 0;
            aikaLaskuri.Reset();
            Aika.Content = 0;
        }


        /// <summary>
        /// Aloittaa uuden pelin. Tähän liitetty myös Aloita uusi peli -painike.
        /// Arpoo peliin luvut ja operaatiot. Jättää yhden labelin tyhjäksi.
        /// </summary>
        /// <param name="target">MainWindow</param>
        /// <param name="e">Tapahtuma, RoutedEvent</param>
        private void UusiPeliExecuted(object target, ExecutedRoutedEventArgs e)
        {
            tyhjennaPeli();

            Random rand = new Random();
            string[] operaatiot = { "+", "-" };

            int luvut = 0;
            int n = rand.Next(3, 5);
            string operaatio = "";

            for (int i = 0; i < n; i++)
            {
                luvut = rand.Next(0, 5);
                funktioLuvut.Add(luvut.ToString());
                montako++;


                if (i < n - 1)
                {
                    operaatio = operaatiot[rand.Next(0, operaatiot.Length)];
                    funktioLuvut.Add(operaatio);
                    montako++;

                }
            }
            //Luodaan arvotuista luvuista funktio.
            string funktio = "";

            foreach (string osa in funktioLuvut)
            {
                funktio += osa;
            }

            DataTable dt = new DataTable();
            var v = dt.Compute(funktio, "");
            Tulos.Content = v.ToString();
            funktioLuvut.Add("=");

            int tyhjanIndeksi = rand.Next(0, funktioLuvut.Count);

            funktioLuvut.RemoveAt(tyhjanIndeksi);

            for (int i = 0; i < funktioLuvut.Count; i++)
            {
                uusiLabel = new RaahattuLabel();
                uusiLabel.Content = funktioLuvut[i].ToString();
                Canvas.SetTop(uusiLabel, rand.Next((int)(Lukualue.ActualHeight - uusiLabel.Height)));
                Canvas.SetLeft(uusiLabel, rand.Next((int)(Lukualue.ActualWidth - uusiLabel.Width)));
                Lukualue.Children.Add(uusiLabel);
            }

            tyhjaLabel = new RaahattuLabel();
            tyhjaLabel.Content = "";
            Canvas.SetTop(tyhjaLabel, rand.Next((int)(Lukualue.ActualHeight - tyhjaLabel.Height)));
            Canvas.SetLeft(tyhjaLabel, rand.Next((int)(Lukualue.ActualWidth - tyhjaLabel.Width)));

            Lukualue.Children.Add(tyhjaLabel);

            tyhjaLabel.MouseDoubleClick += new MouseButtonEventHandler(textBox_MouseDoubleClick);

            montako++;

            aikaLaskuri.Start();
        }


        /// <summary>
        /// Tarkistaa, voiko suorittaa uuden pelin lisäyksen. Voidaan suorittaa aina.
        /// </summary>
        /// <param name="sender">MainWindow</param>
        /// <param name="e">Tapahtuma, RoutedEvent</param>
        private void UusiPeliCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }


        /// <summary>
        /// Aliohjelma, joka suoritetaan silloin, kun siirretään labeleita Tulosalueelle eli Dockpaneliin.
        /// </summary>
        /// <param name="sender">Dockpanel</param>
        /// <param name="e">Tapahtuma</param>
        private void panel_Drop(object sender, DragEventArgs e)
        {

            if (e.Data.GetDataPresent("Label"))
            {
                RaahattuLabel txt = e.Data.GetData("Label") as RaahattuLabel;
                Panel panel = txt.Parent as Panel;

                int mihin = Tulosalue.Children.IndexOf((UIElement)e.Source);
                int mita = Tulosalue.Children.IndexOf(txt);
                //Point point = e.GetPosition((DockPanel)sender);
                //double X = point.X;
                //int indeksi = ((((int)X + 20) / 40));
                point = e.GetPosition((DockPanel)sender);
                X = point.X;
                indeksi = ((((int)X + 20) / 40));

                if (panel == DragAndDrop.DragSource)
                {
                    DockPanel dp = (DockPanel)sender;

                    if (raahatutLabelit.Count < indeksi)
                    {
                        indeksi = raahatutLabelit.Count;
                        panel.Children.Remove(txt);
                        dp.Children.Add(txt);
                        raahatutLabelit.Add(txt);
                    }

                    else
                    {
                        panel.Children.Remove(txt);
                        dp.Children.Insert(indeksi, txt);
                        raahatutLabelit.Insert(indeksi, txt);
                    }

                    montako--;

                    viimeinen++;
                    txt.Uid = indeksi.ToString();
                }

                else
                {
                    DockPanel dp = (DockPanel)sender;

                    if (mita < mihin)
                    {
                        mihin--;
                        panel.Children.RemoveAt(mita);
                        raahatutLabelit.RemoveAt(mita);
                        dp.Children.Insert(mihin, txt);
                        raahatutLabelit.Insert(mihin, txt);
                    }

                    if (mihin == -1)
                    {
                        panel.Children.RemoveAt(mita);
                        raahatutLabelit.RemoveAt(mita);
                        dp.Children.Add(txt);
                        mihin = raahatutLabelit.Count - 1;
                        raahatutLabelit.Add(txt);
                    }

                    else if (mita > mihin)
                    {
                        panel.Children.RemoveAt(mita);
                        raahatutLabelit.Remove(txt);
                        dp.Children.Insert(mihin, txt);
                        raahatutLabelit.Insert(mihin, txt);
                    }

                    txt.Uid = indeksi.ToString();
                }
            }

            if (montako == 0 & raahatutLabelit[viimeinen].Content.ToString() == "=")
            {
                tulosLaskenta();
            }

        }


        /// <summary>
        /// Aliohjelma, joka tarkistaa onko laskun/pelin tulos oikein. 
        /// </summary>
        private void tulosLaskenta()
        {
            string lasku = "";

            for (int i = 0; i < raahatutLabelit.Count - 1; i++)
            {
                lasku = lasku + raahatutLabelit[i].Content.ToString();
            }

            DataTable dt = new DataTable();
            var pelaajanTulos = dt.Compute("1 + 2 - 3", "");
            try
            {
                pelaajanTulos = dt.Compute(lasku, "");
            }

            catch (Exception)
            {
                return;
            }

            int pelaaja = int.Parse(pelaajanTulos.ToString());
            int oikeaTulos = int.Parse(Tulos.Content.ToString());

            if (pelaaja == oikeaTulos)
            {
                Tulosalue.Background = Brushes.Green;
                Oikein.Content = "Oikein!";
                aikaLaskuri.Stop();
                Aika.Content = Math.Round(aikaLaskuri.Elapsed.TotalSeconds, 0) + " s";
            }
        }


        /// <summary>
        /// Aliohjelma, joka suoritetaan silloin, kun siirretään labeleita Lukualueelle eli Canvakseen.
        /// </summary>
        /// <param name="sender">Canvas</param>
        /// <param name="e">Tapahtuma</param>
        private void canvas_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("Label"))
            {
                Label txt = e.Data.GetData("Label") as Label;
                Panel panel = txt.Parent as Panel;
                Canvas canvas = (Canvas)sender;

                if (panel == DragAndDrop.DragSource)
                {
                    Point point = e.GetPosition(DragAndDrop.DragSource);
                    DragAndDrop.DragSource.Children.Remove(txt);
                    Canvas.SetTop(txt, point.Y);
                    Canvas.SetLeft(txt, point.X - (txt.Width));
                    canvas.Children.Add(txt);
                }

                else
                {
                    panel.Children.Remove(txt);
                    canvas.Children.Add(txt);
                    raahatutLabelit.RemoveAt(int.Parse(txt.Uid));
                    viimeinen--;
                    montako++;

                    for (int i = 0; i < raahatutLabelit.Count; i++)
                    {
                        raahatutLabelit[i].Uid = i.ToString();
                    }
                }
            }
        }


        /// <summary>
        /// Suorittaa undo -tapahtuma eli siirtää siirretyn labelin takaisin Canvakselle.
        /// </summary>
        /// <param name="target">MainWindow</param>
        /// <param name="e">tapahtuma, joka suoritetaan</param>
        void UndoCmdExecuted(object target, ExecutedRoutedEventArgs e)
        {
            String command, targetobj;
            command = ((RoutedCommand)e.Command).Name;
            targetobj = ((FrameworkElement)target).Name;

            string sisalto = (raahatutLabelit[viimeinen].Content.ToString());
            raahatutLabelit.RemoveAt(viimeinen);
            viimeinen--;
            montako++;
            if (Tulosalue.Children.Count != 0)
            {
                Tulosalue.Children.RemoveAt(Tulosalue.Children.Count - 1);
                uusiLabel = new RaahattuLabel();
                uusiLabel.Content = sisalto;
                Canvas.SetTop(uusiLabel, rand.Next((int)(Lukualue.ActualHeight - uusiLabel.Height)));
                Canvas.SetLeft(uusiLabel, rand.Next((int)(Lukualue.ActualWidth - uusiLabel.Width)));
                Lukualue.Children.Add(uusiLabel);
            }
        }


        /// <summary>
        /// Tarkistaa, voidaanko undo-tapahtuma suorittaa. Voidaan suorittaa, jos enintään yksi labeli on Tulosalueella.
        /// </summary>
        /// <param name="sender">MainWindow</param>
        /// <param name="e">Tapahtuma</param>
        void UndoCmdCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (raahatutLabelit.Count > 0) e.CanExecute = true;
        }


        /// <summary>
        /// LaskuriProperty, joka päivittyy labeleiden raahauksen mukaan.
        /// </summary>
        public static readonly DependencyProperty LaskuriProperty =
         DependencyProperty.Register(
           "Laskuri",
           typeof(int), // propertyn tietotyyppi
           typeof(MainWindow), // luokka jossa property sijaitsee
           new FrameworkPropertyMetadata(0,  // propertyn oletusarvo
                FrameworkPropertyMetadataOptions.AffectsRender, // vaikuttaa luokan ulkoasuun)
                new PropertyChangedCallback(OnValueChanged),  // kutsutaan propertyn arvon muuttumisen jälkeen
                new CoerceValueCallback(MuutaLaskuria))); // kutsutaan ennen propertyn arvon muutosta


        public int Laskuri
        {
            get { return (int)GetValue(LaskuriProperty); }
            set { SetValue(LaskuriProperty, value); }
        }


        /// <summary>
        /// Tapahtuu, ennen laskurin päivittämistä. Tarkistetaan vain, että luku ei ole negatiivinen
        /// </summary>
        /// <param name="element">MainWindow</param>
        /// <param name="value">Laskurin arvo</param>
        /// <returns></returns>
        private static object MuutaLaskuria(DependencyObject element, object value)
        {
            int luku = (int)value;
            if (luku < 0) value = 0;

            return luku;
        }


        /// <summary>
        /// Tapahtuu laskurin päivittämisen jälkeen. Ei käytössä
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        private static void OnValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            // Ei tarvitse
        }


        /// <summary>
        /// RoutedEvent, jolla muutetaan laskurin arvoa.
        /// </summary>
        /// <param name="sender">Mistä tieto tulee, Grid</param>
        /// <param name="e">tapahtuma</param>
        private void Window_Raahaus(object sender, RoutedEventArgs e)
        {
            Laskuri++;
        }


        /// <summary>
        /// Aliohjelma, jolla muutetaan tyhjä label textboxiksi, jotta voidaan otta syöte.
        /// </summary>
        /// <param name="sender">tuplaklikattu label</param>
        /// <param name="e">tapahtuma</param>
        private void textBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Panel panel = tyhjaLabel.Parent as Panel;

            top = Canvas.GetTop((RaahattuLabel)sender);
            left = Canvas.GetLeft((RaahattuLabel)sender);

            tekstikentta.Height = 40;
            tekstikentta.Width = 40;
            tekstikentta.BorderThickness = new Thickness(0);
            tekstikentta.Background = Brushes.Black;
            tekstikentta.Foreground = Brushes.Yellow;
            tekstikentta.HorizontalContentAlignment = HorizontalAlignment.Center;
            tekstikentta.VerticalContentAlignment = VerticalAlignment.Center;
            tekstikentta.FontSize = 18;
            tekstikentta.FontWeight = FontWeights.Bold;
            tekstikentta.Text = "";
            Canvas.SetTop(tekstikentta, top);
            Canvas.SetLeft(tekstikentta, left);

            if (tyhjaLabel.Parent == DragAndDrop.DragSource)
            {
                panel.Children.Add(tekstikentta);
            }

            else return;

            tekstikentta.KeyDown += new KeyEventHandler(CheckNappain);
        }


        /// <summary>
        /// Tapahtuma, jolla poistutaan textboxista enter, esc tai vasemmalla nuolinäppäimillä
        /// </summary>
        /// <param name="sender">tapahtuman lähettäjä, näppäimistön nappi</param>
        /// <param name="e">tapahtuma</param>
        private void CheckNappain(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Panel panel = tyhjaLabel.Parent as Panel;

                if (e.Key == Key.Enter)
                {
                    tekstikentta = (TextBox)sender;
                    tyhjaLabel.Content = tekstikentta.Text;
                    panel.Children.Remove(tekstikentta);
                }
            }
        }
    }


    /// <summary>
    /// RaahattuLabel -luokka, joka peritty Labelista.
    /// </summary>
    public class RaahattuLabel : Label
    {

        /// <summary>
        /// Konstruktori
        /// </summary>
        public RaahattuLabel()
        {
            this.Background = Brushes.Black;
            this.Foreground = Brushes.Yellow;
            this.FontSize = 18;
            this.FontWeight = FontWeights.Bold;
            this.Width = 40;
            this.Height = 40;
            this.HorizontalContentAlignment = HorizontalAlignment.Center;
            this.VerticalContentAlignment = VerticalAlignment.Center;
            this.Uid = "";
        }


        /// <summary>
        /// RoutedEventin perusversio, ei liikutella mitän omaa dataa.
        /// </summary>
        public static readonly RoutedEvent RaahausEvent =
    EventManager.RegisterRoutedEvent("Raahaus", RoutingStrategy.Bubble,
    typeof(RoutedEventHandler), typeof(RaahattuLabel));

        public event RoutedEventHandler Raahaus
        {
            add { AddHandler(RaahausEvent, value); }
            remove { RemoveHandler(RaahausEvent, value); }
        }
    }

    /// <summary>
    /// Tarkistaa, onko textboxiin syötetty vain numeroita tai +,-,=.
    /// </summary>
    public static class LuvutOperaatiotBehaviour
    {
        public static bool GetLuvutOperaatiot(DependencyObject obj)
        {
            return (bool)obj.GetValue(LuvutOperaatiotProperty);
        }

        public static void SetLuvutOperaatiot(DependencyObject obj, bool value)
        {
            obj.SetValue(LuvutOperaatiotProperty, value);
        }

        public static readonly DependencyProperty LuvutOperaatiotProperty =
          DependencyProperty.RegisterAttached("LuvutOperaatiot",
          typeof(bool), typeof(LuvutOperaatiotBehaviour),
          new UIPropertyMetadata(false, OnLuvutOperaatiotChanged));


        /// <summary>
        /// Tarkistaa, onko sender textbox ja syötön tarkistus käytössä.
        /// </summary>
        /// <param name="sender">Textbox</param>
        /// <param name="e">tapahtuma</param>
        private static void OnLuvutOperaatiotChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is TextBox)) return;

            TextBox textBox = (TextBox)sender;
            bool isLuvutOperaatiot = (bool)(e.NewValue);

            if (isLuvutOperaatiot)
                textBox.PreviewTextInput += OikeaSyote;
            else
                textBox.PreviewTextInput -= OikeaSyote;
        }


        /// <summary>
        /// Tarkistaa, onko syöttö oikeanlainen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OikeaSyote(object sender, TextCompositionEventArgs e)
        {
            foreach (char ch in e.Text)
                if (!Char.IsDigit(ch) && !(ch.Equals('+')) && !(ch.Equals('-')) && !(ch.Equals('=')))
                    e.Handled = true;
        }
    }


    /// <summary>
    /// Luokka omaa Undo-Commandi määrittelyä varten
    /// </summary>
    public static class UndoCommands
    {
        public static readonly RoutedUICommand Undo = new RoutedUICommand
                (
                        "Undo",
                        "Undo",
                        typeof(UndoCommands)
                );

    }


    /// <summary>
    /// Luokka Drag and Dropiin. Drag tehty vain Attached Propertyna.
    /// </summary>
    public class DragAndDrop
    {
        static public Canvas DragSource;
        static public Type DragType;

        //DragEnabled
        public static readonly DependencyProperty DragEnabledProperty =
            DependencyProperty.RegisterAttached("DragEnabled",
                typeof(Boolean),
                typeof(DragAndDrop),
                new FrameworkPropertyMetadata(OnDragEnabledChanged));

        public static void SetDragEnabled(DependencyObject element, Boolean value)
        {
            element.SetValue(DragEnabledProperty, value);
        }

        public static Boolean GetDragEnabled(DependencyObject element)
        {
            return (Boolean)element.GetValue(DragEnabledProperty);
        }


        /// <summary>
        /// Suoritetaan, jos drag mahdollista. Asettaa senderille PreviewMouseLeftButtonDown tapahtuman. 
        /// </summary>
        /// <param name="sender">Canvas</param>
        /// <param name="e">tapahtuma</param>
        public static void OnDragEnabledChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(sender is Canvas)) return;

            Canvas canvas = (Canvas)sender;
            bool isCanvasOnly = (bool)(e.NewValue);

            if (isCanvasOnly)
            {
                DragSource = (Canvas)sender;
                canvas.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(canvas_PreviewMouseLeftButtonDown);
            }
        }


        /// <summary>
        /// Tapahtuu, kun hiiri liikkuu. Laukaistaan labelin oma raahausevent. Kutsutaan myös drop -tapahtumaa.
        /// </summary>
        /// <param name="sender">label</param>
        /// <param name="e">tapahtuma</param>
        private static void label_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DataObject data = new DataObject();
                data.SetData("Label", sender);
                data.SetData(DataFormats.StringFormat, ((Label)sender).Content);

                Label label = (Label)sender;
                RoutedEventArgs newEventArgs = new RoutedEventArgs(RaahattuLabel.RaahausEvent);
                label.RaiseEvent(newEventArgs);

                System.Windows.DragDrop.DoDragDrop(DragSource, data, DragDropEffects.Move);
            }
        }


        /// <summary>
        /// Tapahtuu, kun hiiren vasenta -painiketta painettu. Laitetaan mousemove tapahtuma.
        /// </summary>
        /// <param name="sender">canvas/dockpanel</param>
        /// <param name="e">tapahtuma</param>
        static void canvas_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.Source == (Canvas)sender) return;
            try
            {
                if (e.Source == (TextBox)e.Source) return;
            }

            catch (Exception)
            {
                //
            }

            RaahattuLabel label = (RaahattuLabel)e.Source;

            label.MouseMove += new MouseEventHandler(label_MouseMove);
        }
    }
}

