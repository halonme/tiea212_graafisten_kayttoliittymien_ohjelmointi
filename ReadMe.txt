Kesällä 2017 itsenäisesti suoritetun yliopiston ohjelmistotekniikan Graafisten käyttöliittymien ohjelmointi -kurssin työnäytteitä. Kurssilla opeteltiin ohjelmointia Windows-ympäristössä ja komponenttipohjaista ohjelmointia. 

Työnäytteissä on kaksi kurssin aikana C#:lla ja WPF:llä toteutettua pientä sovellusta (Siirtelypeli, WPFTestaulua) sekä hieman laajempi harjoitustyö (Myllypeli).